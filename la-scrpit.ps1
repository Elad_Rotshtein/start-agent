#starts an instance if it is stopped, waits 30 secconds if it is not stopped, running or made 8 sleeps, throws error after 8 sleeps and throws error if the instance is terminated
 
$ErrorActionPreference = 'stop'

$Seconds = 30 #seconds to sleep

$InstanceID = 'i-065f942c5f0298b92' #id of instance to run on
Write-Host "Running on the instace ID $InstanceID"

function Get-Ec2InstanceState($InstanceId) {
    $json = cmd.exe /c "aws ec2 describe-instance-status --instance-ids $InstanceId" | ConvertFrom-Json
    $state = $json.InstanceStatuses.instancestate.name
    if ($state -eq $null) {$state = 'stopped'}
    return $state
}

$attempts = 0
$sleeps = 0
while ($(Write-Host "Getting the state of the instance"; $state = Get-Ec2InstanceState $InstanceID; $state) -ne 'running') {
    Write-Host "The instance is in the state $state"

    if ($state -eq 'stopped') {
        if ($attempts -gt 6) {
            Write-Error -ErrorAction Stop -Message "6 attempts have been made to start the instance '$InstanceID'."
        }

        Write-Host "Attempting to start the instance"
        try {
            cmd.exe /c "aws ec2 start-instances --instance-ids $InstanceID"
        }
        catch {
            Write-Host "An error occured while attempting to start the instance. Will try again."
        }
        $attempts += 1
        Write-Host "sleeping for 5 seconds after attempting to start the instance"
        Start-Sleep -Seconds 5
    }
    elseif ($state -notin ('shutting-down','terminated')) {
        if ($sleeps -gt 8) {
            Write-Error -ErrorAction Stop -Message "Instance $InstanceID has been in the state $state for more than $($sleeps / 2) minutes!"
        }
        Write-Host "sleeping for $Seconds seconds"
        Start-Sleep -Seconds $Seconds
        $sleeps += 1
    }
    else {
        Write-Error -ErrorAction Stop -Message "The instance $InstanceID is terminated!"
    }
}

Write-Host "The instance $InstanceID is running"